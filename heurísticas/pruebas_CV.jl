## --- LIBRERÍAS ---
using JEMSS
using Statistics

## --- INICIAR SIMULACIÓN BASE ---
simSAMU = initSim("data/base/configs/base_SAMU.xml");

## --- FUNCIÓN LEER LLAMADAS ----
function createCalls(sim=shared_sim, filepath::String="../valencia_metro/data/calls/2019/2019.csv")::Vector{Call}
    """
    Creates calls for the simulation

    sim: Simulation of reference.
    filepath: Path to the file with the calls. Must be a csv file.
    """

    allCalls, _=readCallsFile(filepath)
    for c in allCalls 
        if c.nearestNodeIndex==nullIndex
            (c.nearestNodeIndex, c.nearestNodeDist) = findNearestNode(sim.map, sim.grid, sim.net.fGraph.nodes, c.location)
        end
    end

    return allCalls
end 

## --- FUNCIÓN AMBULANCIAS ---
function createAmbulances(v_ambulances)
    n = length(v_ambulances)
    ambulances = Vector{Ambulance}(undef, n)

    for i in 1:n
        ambulances[i] = Ambulance()
        ambulances[i].index = i
        ambulances[i].class = AmbClass(1)
        ambulances[i].stationIndex = v_ambulances[i]
    end

    return ambulances
end

## --- CONJUNTO TRAIN - TEST
enero = [1, 7, 8, 12, 18, 23, 25, 27, 31]; ene = setdiff(1:31, enero);
febrero = [3, 5, 8, 9, 13, 18, 19, 27]; feb = setdiff(1:28, febrero);
marzo = [2, 6, 8, 14, 16, 20, 24, 25, 28]; mar = setdiff(1:31, marzo);
abril = [1, 5, 10, 11, 16, 20, 28, 29, 30]; abr = setdiff(1:30, abril);
mayo = [2, 8, 12, 13, 17, 19, 23, 28, 31]; may = setdiff(1:31, mayo);
junio = [3, 7, 9, 12, 13, 19, 22, 25, 29]; jun = setdiff(1:30, junio);
julio = [1, 6, 7, 9, 18, 21, 25, 26, 30]; jul = setdiff(1:31, julio);
agosto = [2, 6, 11, 12, 15, 20, 21, 25, 28, 30]; ago = setdiff(1:31, agosto);
septiembre = [1, 6, 7, 9, 14, 18, 26, 27, 30]; sep = setdiff(1:30, septiembre);
octubre = [3, 7, 9, 13, 15, 17, 19, 25, 26, 30]; oct = setdiff(1:31, octubre);
noviembre = [2, 5, 9, 13, 15, 18, 21, 24, 25]; nov = setdiff(1:30, noviembre);
diciembre = [1, 5, 12, 15, 17, 23, 24, 28, 30]; dic = setdiff(1:31, diciembre);

meses_test = [enero,febrero,marzo,abril,mayo,junio,julio,agosto,septiembre,octubre,noviembre,diciembre];
meses_train = [ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic];

## --- datos para validación cruzada
using Random

# Definir los nombres de los meses
months = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];

# Crear un diccionario que asocie el nombre del mes con el arreglo correspondiente
month_dict = Dict("ene" => ene, "feb" => feb, "mar" => mar, "abr" => abr, "may" => may, "jun" => jun, "jul" => jul, "ago" => ago, "sep" => sep, "oct" => oct, "nov" => nov, "dic" => dic);

# lista con los distintos conjuntos
lista_datosVC = Vector{Any}[];

for m in months
    if m == "feb"
        x = 5
    else
        x = 6
    end
    
    # Obtener el arreglo correspondiente al mes actual
    current_array = month_dict[m]
    
    # Realizar operaciones con current_array en lugar de ene
    ind1 = randperm(length(current_array))[1:5]; arr1 = current_array[ind1]
    ind2 = randperm(length(setdiff(current_array, arr1)))[1:5]; arr2 = setdiff(current_array, arr1)[ind2]
    ind3 = randperm(length(setdiff(current_array, vcat(arr1, arr2))))[1:x]; arr3 = setdiff(current_array, vcat(arr1, arr2))[ind3]
    arr4 = setdiff(current_array, vcat(arr1, arr2, arr3))
    
    push!(lista_datosVC, arr1, arr2, arr3, arr4)
end

ene1=lista_datosVC[1]; ene2=lista_datosVC[2]; ene3=lista_datosVC[3]; ene4=lista_datosVC[4];
feb1=lista_datosVC[5]; feb2=lista_datosVC[6]; feb3=lista_datosVC[7]; feb4=lista_datosVC[8];
mar1=lista_datosVC[9]; mar2=lista_datosVC[10]; mar3=lista_datosVC[11]; mar4=lista_datosVC[12];
abr1=lista_datosVC[13]; abr2=lista_datosVC[14]; abr3=lista_datosVC[15]; abr4=lista_datosVC[16];
may1=lista_datosVC[17]; may2=lista_datosVC[18]; may3=lista_datosVC[19]; may4=lista_datosVC[20];
jun1=lista_datosVC[21]; jun2=lista_datosVC[22]; jun3=lista_datosVC[23]; jun4=lista_datosVC[24];
jul1=lista_datosVC[25]; jul2=lista_datosVC[26]; jul3=lista_datosVC[27]; jul4=lista_datosVC[28];
ago1=lista_datosVC[29]; ago2=lista_datosVC[30]; ago3=lista_datosVC[31]; ago4=lista_datosVC[32];
sep1=lista_datosVC[33]; sep2=lista_datosVC[34]; sep3=lista_datosVC[35]; sep4=lista_datosVC[36];
oct1=lista_datosVC[37]; oct2=lista_datosVC[38]; oct3=lista_datosVC[39]; oct4=lista_datosVC[40];
nov1=lista_datosVC[41]; nov2=lista_datosVC[42]; nov3=lista_datosVC[43]; nov4=lista_datosVC[44];
dic1=lista_datosVC[45]; dic2=lista_datosVC[46]; dic3=lista_datosVC[47]; dic4=lista_datosVC[48];

meses_testCV1 = [ene1,feb1,mar1,abr1,may1,jun1,jul1,ago1,sep1,oct1,nov1,dic1];
meses_testCV2 = [ene2,feb2,mar2,abr2,may2,jun2,jul2,ago2,sep2,oct2,nov2,dic2];
meses_testCV3 = [ene3,feb3,mar3,abr3,may3,jun3,jul3,ago3,sep3,oct3,nov3,dic3];
meses_testCV4 = [ene4,feb4,mar4,abr4,may4,jun4,jul4,ago4,sep4,oct4,nov4,dic4];
conjuntos_test = [meses_testCV1, meses_testCV2, meses_testCV3, meses_testCV4];

meses_trainCV1 =[setdiff(ene,ene1),setdiff(feb,feb1),setdiff(mar,mar1),setdiff(abr,abr1),setdiff(may,may1),setdiff(jun,jun1),setdiff(jul,jul1),setdiff(ago,ago1),setdiff(sep,sep1),setdiff(oct,oct1),setdiff(nov,nov1),setdiff(dic,dic1)];
meses_trainCV2 =[setdiff(ene,ene2),setdiff(feb,feb2),setdiff(mar,mar2),setdiff(abr,abr2),setdiff(may,may2),setdiff(jun,jun2),setdiff(jul,jul2),setdiff(ago,ago2),setdiff(sep,sep2),setdiff(oct,oct2),setdiff(nov,nov2),setdiff(dic,dic2)];
meses_trainCV3 =[setdiff(ene,ene3),setdiff(feb,feb3),setdiff(mar,mar3),setdiff(abr,abr3),setdiff(may,may3),setdiff(jun,jun3),setdiff(jul,jul3),setdiff(ago,ago3),setdiff(sep,sep3),setdiff(oct,oct3),setdiff(nov,nov3),setdiff(dic,dic3)];
meses_trainCV4 =[setdiff(ene,ene4),setdiff(feb,feb4),setdiff(mar,mar4),setdiff(abr,abr4),setdiff(may,may4),setdiff(jun,jun4),setdiff(jul,jul4),setdiff(ago,ago4),setdiff(sep,sep4),setdiff(oct,oct4),setdiff(nov,nov4),setdiff(dic,dic4)];
conjuntos_train = [meses_trainCV1, meses_trainCV2, meses_trainCV3, meses_trainCV4];


## --- HEURÍSTICA con validación cruzada ---

# 1. Calcular la demanda de llamadas de cada base de emergencia
using CSV, DataFrames

### ---- VALIDACIÓN CRUZADA
valores_N=1:15; #posibles valores de N para probar

num_pliegues = 4; # Definir el número de pliegues

# Inicializar el rendimiento
rendimientos = DataFrame(N=zeros(Int64, 15), fold1=zeros(Float64, 15), fold2=zeros(Float64, 15), fold3=zeros(Float64, 15), fold4=zeros(Float64, 15), promedio=zeros(Float64, 15));# Crear un DataFrame vacío con nombres de columnas



# Realizar la validación cruzada
for N in valores_N
    rendimientos.N[N]=N;

    for i in 1:num_pliegues
        
        tren_data = conjuntos_train[i]
        prueba_data = conjuntos_test[i]
        
        ## estaciones
        allStations = CSV.File("data/stations/all_stations.csv") |> DataFrame;
        coordinates_stations = select(allStations, [:ID, :x, :y]);

        # vector de estaciones más cercanas a las llamadas
        indice_estacion_cercana = Vector{Int}();

        for month in 1:12
            for day in tren_data[month]
                # Construir la ruta del archivo CSV
                file_path = joinpath(base_path, string(month, "/", day, "/2019-", month, "-", day, "_SAMU.csv"))
        
                # Verificar si el archivo existe antes de intentar cargarlo
                if isfile(file_path)
                    ## llamadas de emergencia (conjunto train)
                    calls = CSV.File(file_path; header=6) |> DataFrame;
                    coordinates_calls = select(calls, [:x, :y]);
        
                    ## distancia euclidiana entre coordenadas
                    for llamada in eachrow(coordinates_calls)
                        distancias = [sqrt((llamada.x - estacion.x)^2 + (llamada.y - estacion.y)^2) for estacion in eachrow(coordinates_stations)]
                        push!(indice_estacion_cercana, argmin(distancias))
                    end
                end
            end
        
        end
        
        # Contar la cantidad de llamadas por estación
        frecuencias_df = combine(groupby(DataFrame(ID = indice_estacion_cercana), :ID), nrow);
        rename!(frecuencias_df, :nrow => :calls); # Renombrar la columna de recuento
        coordinates_stations = leftjoin(coordinates_stations, frecuencias_df, on=:ID); # Fusionar la información de frecuencia con coordinates_stations
        dropmissing!(coordinates_stations, :calls);

        # 2. Heurística

        bases_objetivo = 10; #numero de bases que quiero seleccionar
        
        bases_finales = DataFrame();

        list_stations=copy(coordinates_stations); # crear una lista con todas las estaciones

        for base in 1:bases_objetivo
            push!(bases_finales, list_stations[argmax(list_stations.calls),:]); # coger la estacion con más llamadas
            delete!(list_stations, argmax(list_stations.calls)); # eliminarla de la lista

            for n in 1:N
                ## distancia euclidiana entre coordenadas
                distancias = [sqrt((bases_finales[base,:].x - estacion.x)^2 + (bases_finales[base,:].y - estacion.y)^2) for estacion in eachrow(list_stations)]
                delete!(list_stations, argmin(distancias))
            end
        end

        # 3. Calcular tiempo respuesta (conjunto test)

        # Bucle para recorrer todos los días del conjunto test

        # Configuración ambulancias SAMU
        v_ambulances=bases_finales.ID;

        # Variable para almacenar todas las mean_resp_time
        all_mean_resp_times = [];

        for month in 1:12
            for day in prueba_data[month]  # Puedes ajustar esto según el número máximo de días en cada mes
                # Construir la ruta del archivo CSV
                file_path = joinpath(base_path, string(month, "/", day, "/2019-", month, "-", day, "_SAMU.csv"))

                # Verificar si el archivo existe antes de intentar cargarlo
                if isfile(file_path)
                    # Crear llamadas y simular
                    callstest = createCalls(simSAMU, file_path)
                    ambulances = createAmbulances(v_ambulances)
                    sim = initSim2(simSAMU, callstest, ambulances)
                    simulate!(sim)

                    # Obtener estadísticas
                    tot_resp_time = sim.stats.periods[1].call.totalResponseDuration
                    num_calls = sim.numCalls
                    mean_resp_time = tot_resp_time / num_calls * (24 * 60)

                    # Almacenar mean_resp_time en la lista
                    push!(all_mean_resp_times, mean_resp_time)

                end
            end

        end
        # Calcular la media de todas las mean_resp_time
        rendimientos[N,i+1] = mean(all_mean_resp_times)
    end
end

rendimientos.promedio= [mean(x) for x in eachrow(rendimientos[:, 2:5])];


