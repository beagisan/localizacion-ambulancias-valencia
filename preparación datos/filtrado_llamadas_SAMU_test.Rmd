---
title: "2019_SAMU_test"
output: html_document
date: "2023-09-28"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Días test
## días de test cada mes

```{r}
enero <- c(1,7,8,12,18,23,25,27,31)
ene <- setdiff(1:31, enero)
febrero <- c(3,5,8,9,13,18,19,27)
feb <- setdiff(1:28, febrero)
marzo <- c(2,6,8,14,16,20,24,25,28)
mar <- setdiff(1:31, marzo)
abril <- c(1,5,10,11,16,20,28,29,30)
abr <- setdiff(1:30, abril)
mayo <- c(2,8,12,13,17,19,23,28,31)
may <- setdiff(1:31, mayo)
junio <- c(3,7,9,12,13,19,22,25,29)
jun <- setdiff(1:30, junio)
julio <- c(1,6,7,9,18,21,25,26,30)
jul <- setdiff(1:31, julio)
agosto <- c(2,6,11,12,15,20,21,25,28,30)
ago <- setdiff(1:31, agosto)
septiembre <- c(1,6,7,9,14,18,26,27,30)
sep <- setdiff(1:30, septiembre)
octubre <- c(3,7,9,13,15,17,19,25,26,30)
oct <- setdiff(1:31, octubre)
noviembre <- c(2,5,9,13,15,18,21,24,25)
nov <- setdiff(1:30, noviembre)
diciembre <- c(1,5,12,15,17,23,24,28,30)
dic <- setdiff(1:31, diciembre)
```

```{r}
meses <- list(enero,febrero,marzo,abril,mayo,junio,julio,agosto,septiembre,octubre,noviembre,diciembre)

for (j in 1:12){
  
  # Especifica el patrón de nombre de archivo común
  patron_nombre <- paste0("2019-", j, "-")
  
  # Crear un data frame vacío
  test_mes<- data.frame(Columna1 = numeric(0), Columna2 = character(0))
  
  for (i in meses[[j]]){
    # Genera el nombre completo del archivo CSV
    nombre_archivo <- paste0(patron_nombre, i, "_SAMU.csv")
    ruta_archivo <- file.path(paste0(j,"/",i), nombre_archivo)
    
     # quedarme solo con las llamadas de los días de test
    ll = read.csv(ruta_archivo, header=FALSE, as.is=TRUE)
    ll_tab = ll[7:nrow(ll),]
    test_mes <- rbind(test_mes, ll_tab)
    test_mes$V1=1:nrow(test_mes)
  }
  ini_ll <- ll[2:6, ]  # Tomar las 6 primeras filas del archivo original
  test_mes <- rbind(ini_ll, test_mes)
  colnames(test_mes) <- ll[1,]
  
  # guardar archivo csv
  patron_nombre2 <- paste0("2019-", j)
  nombre_archivo_nuevo <- paste0(patron_nombre2, "_SAMU_test.csv")
  ruta_completa <- file.path(j, nombre_archivo_nuevo)  # ruta completa al archivo
  write.csv(test_mes, file = ruta_completa, row.names = FALSE)
}

```

## año entero

```{r}
# Crear un data frame vacío
test_2019<- data.frame(Columna1 = numeric(0), Columna2 = character(0))

for (j in 1:12){
  # Especifica el patrón de nombre de archivo común
  patron_nombre <- paste0("2019-", j, "_SAMU_test.csv")

  ruta_archivo <- file.path(j, patron_nombre)
    
  # quedarme solo con las llamadas de los días de test
  ll = read.csv(ruta_archivo, header=FALSE, as.is=TRUE)
  ll_tab = ll[7:nrow(ll),]
  test_2019 <- rbind(test_2019, ll_tab)
  test_2019$V1=1:nrow(test_2019)
}

ini_ll <- ll[2:6, ]  # Tomar las 6 primeras filas del archivo original
test_2019 <- rbind(ini_ll, test_2019)
colnames(test_2019) <- ll[1,]
  
# guardar archivo csv
write.csv(test_2019, file = "2019_SAMU_test.csv", row.names = FALSE)
```

# Días training
## días de test cada mes
```{r}
meses <- list(ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic)

for (j in 1:12){
  
  # Especifica el patrón de nombre de archivo común
  patron_nombre <- paste0("2019-", j, "-")
  
  # Crear un data frame vacío
  train_mes<- data.frame(Columna1 = numeric(0), Columna2 = character(0))
  
  for (i in meses[[j]]){
    # Genera el nombre completo del archivo CSV
    nombre_archivo <- paste0(patron_nombre, i, "_SAMU.csv")
    ruta_archivo <- file.path(paste0(j,"/",i), nombre_archivo)
    
     # quedarme solo con las llamadas de los días de training
    ll = read.csv(ruta_archivo, header=FALSE, as.is=TRUE)
    ll_tab = ll[7:nrow(ll),]
    train_mes <- rbind(train_mes, ll_tab)
    train_mes$V1=1:nrow(train_mes)
  }
  ini_ll <- ll[2:6, ]  # Tomar las 6 primeras filas del archivo original
  train_mes <- rbind(ini_ll, train_mes)
  colnames(train_mes) <- ll[1,]
  
  # guardar archivo csv
  patron_nombre2 <- paste0("2019-", j)
  nombre_archivo_nuevo <- paste0(patron_nombre2, "_SAMU_train.csv")
  ruta_completa <- file.path(j, nombre_archivo_nuevo)  # ruta completa al archivo
  write.csv(train_mes, file = ruta_completa, row.names = FALSE)
}

```

## año entero

```{r}
# Crear un data frame vacío
train_2019<- data.frame(Columna1 = numeric(0), Columna2 = character(0))

for (j in 1:12){
  # Especifica el patrón de nombre de archivo común
  patron_nombre <- paste0("2019-", j, "_SAMU_train.csv")

  ruta_archivo <- file.path(j, patron_nombre)
    
  # quedarme solo con las llamadas de los días de test
  ll = read.csv(ruta_archivo, header=FALSE, as.is=TRUE)
  ll_tab = ll[7:nrow(ll),]
  train_2019 <- rbind(train_2019, ll_tab)
  train_2019$V1=1:nrow(train_2019)
}

ini_ll <- ll[2:6, ]  # Tomar las 6 primeras filas del archivo original
train_2019 <- rbind(ini_ll, train_2019)
colnames(train_2019) <- ll[1,]
  
# guardar archivo csv
write.csv(train_2019, file = "2019_SAMU_train.csv", row.names = FALSE)
```

### Comprobacion

```{r}

llam <- c()
for (i in octubre){
  # Genera el nombre completo del archivo CSV
  patron_nombre <- paste0("2019-10-")
  nombre_archivo <- paste0(patron_nombre, i, "_SAMU.csv")
  ruta_archivo <- file.path(paste0(10,"/",i), nombre_archivo)
  ll = read.csv(ruta_archivo, header=FALSE, as.is=TRUE)
  ll_tab = ll[7:nrow(ll),]
  llam <- c(llam, nrow(ll_tab))
}
sum(llam)
enero_SAMU <- read.csv(file.path(10, "2019-10_SAMU_test.csv"), header=FALSE, as.is=TRUE)
nrow(enero_SAMU)-6

llam <- c()
for (i in oct){
  # Genera el nombre completo del archivo CSV
  patron_nombre <- paste0("2019-10-")
  nombre_archivo <- paste0(patron_nombre, i, "_SAMU.csv")
  ruta_archivo <- file.path(paste0(10,"/",i), nombre_archivo)
  ll = read.csv(ruta_archivo, header=FALSE, as.is=TRUE)
  ll_tab = ll[7:nrow(ll),]
  llam <- c(llam, nrow(ll_tab))
}
sum(llam)
enero_SAMU <- read.csv(file.path(10, "2019-10_SAMU_train.csv"), header=FALSE, as.is=TRUE)
nrow(enero_SAMU)-6
```

