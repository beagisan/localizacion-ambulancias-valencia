## --- LIBRERÍAS ---
using JEMSS
using Statistics
using DataFrames
using StatsBase

## --- INICIAR SIMULACIÓN BASE ---
simSAMU = initSim("data/base/configs/base_SAMU.xml");

## --- FUNCIÓN LEER LLAMADAS ----
function createCalls(sim=shared_sim, filepath::String="../valencia_metro/data/calls/2019/2019.csv")::Vector{Call}
    """
    Creates calls for the simulation

    sim: Simulation of reference.
    filepath: Path to the file with the calls. Must be a csv file.
    """

    allCalls, _=readCallsFile(filepath)
    for c in allCalls 
        if c.nearestNodeIndex==nullIndex
            (c.nearestNodeIndex, c.nearestNodeDist) = findNearestNode(sim.map, sim.grid, sim.net.fGraph.nodes, c.location)
        end
    end

    return allCalls
end 

## --- FUNCIÓN AMBULANCIAS ---
function createAmbulances(v_ambulances)
    n = length(v_ambulances)
    ambulances = Vector{Ambulance}(undef, n)

    for i in 1:n
        ambulances[i] = Ambulance()
        ambulances[i].index = i
        ambulances[i].class = AmbClass(1)
        ambulances[i].stationIndex = v_ambulances[i]
    end

    return ambulances
end

## --- FUNCIÓN OBTENER TIEMPO DE RESPUESTA DÍAS TEST
function response_time(v_ambulances)
    enero = [1, 7, 8, 12, 18, 23, 25, 27, 31]
    febrero = [3, 5, 8, 9, 13, 18, 19, 27]
    marzo = [2, 6, 8, 14, 16, 20, 24, 25, 28]
    abril = [1, 5, 10, 11, 16, 20, 28, 29, 30]
    mayo = [2, 8, 12, 13, 17, 19, 23, 28, 31]
    junio = [3, 7, 9, 12, 13, 19, 22, 25, 29]
    julio = [1, 6, 7, 9, 18, 21, 25, 26, 30]
    agosto = [2, 6, 11, 12, 15, 20, 21, 25, 28, 30]
    septiembre = [1, 6, 7, 9, 14, 18, 26, 27, 30]
    octubre = [3, 7, 9, 13, 15, 17, 19, 25, 26, 30]
    noviembre = [2, 5, 9, 13, 15, 18, 21, 24, 25]
    diciembre = [1, 5, 12, 15, 17, 23, 24, 28, 30]

    meses_test = [enero,febrero,marzo,abril,mayo,junio,julio,agosto,septiembre,octubre,noviembre,diciembre]
    base_path = "data/calls/2019/"; # Ruta base de los archivos
    all_mean_resp_times = []; # Variable para almacenar todas las mean_resp_time
    for month in 1:12
        for day in meses_test[month]  # Puedes ajustar esto según el número máximo de días en cada mes
            # Construir la ruta del archivo CSV
            file_path = joinpath(base_path, string(month, "/", day, "/2019-", month, "-", day, "_SAMU.csv"))

            # Verificar si el archivo existe antes de intentar cargarlo
            if isfile(file_path)
                # Crear llamadas y simular
                calls = createCalls(simSAMU, file_path)
                ambulances = createAmbulances(v_ambulances)
                sim = initSim2(simSAMU, calls, ambulances)
                simulate!(sim)

                # Obtener estadísticas
                tot_resp_time = sim.stats.periods[1].call.totalResponseDuration
                num_calls = sim.numCalls
                mean_resp_time = tot_resp_time / num_calls * (24 * 60)

                # Almacenar mean_resp_time en la lista
                push!(all_mean_resp_times, mean_resp_time)

            end
        end

    end
    # Calcular la media de todas las mean_resp_time
    return mean(all_mean_resp_times)
end

# Resultado heurística constructiva N=10
heuN10_ambulances = [244,157,141,242,179,191,189,78,61,89]; # N=10
#resp = response_time(heuN10_ambulances);
v_ambulances=heuN10_ambulances; # Configuración ambulancias SAMU

# Registro de vectores y tiempo respuesta
registros = Set{Tuple{Vector{Int}, Float64}}();
push!(registros, (copy(v_ambulances), response_time(v_ambulances)));

# Realizar el intercambio 5 veces y registrar cada vector con su tiempo respuesta
function intercambiar_numeros(vector)
    # Eliminar dos números aleatorios
    numeros_a_eliminar = sample(vector, 2, replace=false)
    vector = filter(x -> !(x in numeros_a_eliminar), vector)

    # Agregar dos números aleatorios que no estén en el vector
    for _ in 1:2
        nuevo_numero = rand(1:277)
        while nuevo_numero in vector
            nuevo_numero = rand(1:277)
        end
        push!(vector, nuevo_numero)
    end

    return vector
end
for i in 1:50
    v_ambulances2 = copy(v_ambulances)
    t_resp=7

    # Solo se aceptan soluciones de tiempo de respuesta menores a 6.5 min
    while (t_resp > 5.75)
        v_ambulances2 = intercambiar_numeros(v_ambulances)

        # Asegurar que el vector no esté en los registros
        if !(v_ambulances2 in getindex.(registros,1))
            t_resp = response_time(v_ambulances2)
        end  
    end
    push!(registros, (copy(v_ambulances2), t_resp))
    v_ambulances=copy(v_ambulances2)
end

# Mostrar los registros
for registro in registros
    println("v: $(registro[1]), t: $(registro[2])")
end