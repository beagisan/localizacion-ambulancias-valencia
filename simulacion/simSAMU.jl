## --- LIBRERÍAS ---
using JEMSS
using Statistics

## --- INICIAR SIMULACIÓN BASE ---
simSAMU = initSim("data/base/configs/base_SAMU.xml");
#simulate!(simSAMU);
#writeOutputFiles(simSAMU)

## --- FUNCIÓN LEER LLAMADAS ----
function createCalls(sim=shared_sim, filepath::String="../valencia_metro/data/calls/2019/2019.csv")::Vector{Call}
    """
    Creates calls for the simulation

    sim: Simulation of reference.
    filepath: Path to the file with the calls. Must be a csv file.
    """

    allCalls, _=readCallsFile(filepath)
    for c in allCalls 
        if c.nearestNodeIndex==nullIndex
            (c.nearestNodeIndex, c.nearestNodeDist) = findNearestNode(sim.map, sim.grid, sim.net.fGraph.nodes, c.location)
        end
    end

    return allCalls
end 

## --- FUNCIÓN AMBULANCIAS ---
function createAmbulances(v_ambulances)
    n = length(v_ambulances)
    ambulances = Vector{Ambulance}(undef, n)

    for i in 1:n
        ambulances[i] = Ambulance()
        ambulances[i].index = i
        ambulances[i].class = AmbClass(1)
        ambulances[i].stationIndex = v_ambulances[i]
    end

    return ambulances
end

## --- SIMULACIÓN todos los días del año ---
# Ruta base de los archivos
base_path = "data/calls/2019/";

# Configuración ambulancias SAMU
v_ambulances=[124, 275, 276, 161, 162, 238, 277, 15, 216, 142]; #configuración actual VLC

# Variable para almacenar todas las mean_resp_time
all_mean_resp_times = [];

# Bucle para recorrer todos los días del año
for month in 1:12
    for day in 1:31  # Puedes ajustar esto según el número máximo de días en cada mes
        # Construir la ruta del archivo CSV
        file_path = joinpath(base_path, string(month, "/", day, "/2019-", month, "-", day, "_SAMU.csv"))

        # Verificar si el archivo existe antes de intentar cargarlo
        if isfile(file_path)
            # Crear llamadas y simular
            calls = createCalls(simSAMU, file_path)
            ambulances = createAmbulances(v_ambulances)
            sim = initSim2(simSAMU, calls, ambulances)
            simulate!(sim)

            # Obtener estadísticas
            tot_resp_time = sim.stats.periods[1].call.totalResponseDuration
            num_calls = sim.numCalls
            mean_resp_time = tot_resp_time / num_calls * (24 * 60)

            # Almacenar mean_resp_time en la lista
            push!(all_mean_resp_times, mean_resp_time)

        end
    end

end
# Calcular la media de todas las mean_resp_time
mean_resp_time = mean(all_mean_resp_times)


### --- SIMULACIÓN conjunto TEST ---
# Días de entrenamiento y test en cada mes
enero = [1, 7, 8, 12, 18, 23, 25, 27, 31]; ene = setdiff(1:31, enero);
febrero = [3, 5, 8, 9, 13, 18, 19, 27]; feb = setdiff(1:28, febrero);
marzo = [2, 6, 8, 14, 16, 20, 24, 25, 28]; mar = setdiff(1:31, marzo);
abril = [1, 5, 10, 11, 16, 20, 28, 29, 30]; abr = setdiff(1:30, abril);
mayo = [2, 8, 12, 13, 17, 19, 23, 28, 31]; may = setdiff(1:31, mayo);
junio = [3, 7, 9, 12, 13, 19, 22, 25, 29]; jun = setdiff(1:30, junio);
julio = [1, 6, 7, 9, 18, 21, 25, 26, 30]; jul = setdiff(1:31, julio);
agosto = [2, 6, 11, 12, 15, 20, 21, 25, 28, 30]; ago = setdiff(1:31, agosto);
septiembre = [1, 6, 7, 9, 14, 18, 26, 27, 30]; sep = setdiff(1:30, septiembre);
octubre = [3, 7, 9, 13, 15, 17, 19, 25, 26, 30]; oct = setdiff(1:31, octubre);
noviembre = [2, 5, 9, 13, 15, 18, 21, 24, 25]; nov = setdiff(1:30, noviembre);
diciembre = [1, 5, 12, 15, 17, 23, 24, 28, 30]; dic = setdiff(1:31, diciembre);

meses_test = [enero,febrero,marzo,abril,mayo,junio,julio,agosto,septiembre,octubre,noviembre,diciembre];
meses_train = [ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic];

# Bucle para recorrer todos los días del conjunto test

# Ruta base de los archivos
base_path = "data/calls/2019/";

# Configuración ambulancias SAMU
v_ambulances=[124, 275, 276, 161, 162, 238, 277, 15, 216, 142]; #configuración actual VLC

# Variable para almacenar todas las mean_resp_time
all_mean_resp_times = [];

for month in 1:12
    for day in meses_test[month]  # Puedes ajustar esto según el número máximo de días en cada mes
        # Construir la ruta del archivo CSV
        file_path = joinpath(base_path, string(month, "/", day, "/2019-", month, "-", day, "_SAMU.csv"))

        # Verificar si el archivo existe antes de intentar cargarlo
        if isfile(file_path)
            # Crear llamadas y simular
            calls = createCalls(simSAMU, file_path)
            ambulances = createAmbulances(v_ambulances)
            sim = initSim2(simSAMU, calls, ambulances)
            simulate!(sim)

            # Obtener estadísticas
            tot_resp_time = sim.stats.periods[1].call.totalResponseDuration
            num_calls = sim.numCalls
            mean_resp_time = tot_resp_time / num_calls * (24 * 60)

            # Almacenar mean_resp_time en la lista
            push!(all_mean_resp_times, mean_resp_time)

        end
    end

end
# Calcular la media de todas las mean_resp_time
mean_resp_time = mean(all_mean_resp_times)